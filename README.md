# NeotechTest

Test task 'Mega Lorem Ipsum' for NEOTECH Company

## Make sure you have installed NodeJS, TypeScript, Angular CLI

## Backend server

Run `npm install` then `npm run dev` from `server` folder. Your backend server has been started at `http://localhost:3000/`

## Frontend server

Run `npm install` then `npm run start` from `src` folder. Navigate to `http://localhost:4200/`
