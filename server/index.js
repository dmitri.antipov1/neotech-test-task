const express = require('express');
const fs = require('fs');
const app = express()
const PORT = process.env.PORT || 3000;
const cors = require('cors');
const dataPath = './mockCountriesData.json'

const readFile = (callback, filePath = dataPath, encoding = 'utf8') => {
  fs.readFile(filePath, encoding, (err, data) => {
    if (err) {
      throw err;
    }
    callback(data);
  });
};

const writeFile = (fileData, callback, filePath = dataPath, encoding = 'utf8') => {
  fs.writeFile(filePath, fileData, encoding, (err) => {
    if (err) {
      throw err;
    }
    callback();
  });
};

app.use(cors())

app.get('/api/countries', (req, res) => {
  readFile((data) => {
    res.send(data);
  });
});

app.get('/api/countries/:id', (req, res) => {
  readFile((data) => {
    const countriesData = JSON.parse(data)
    const countryId = Number(req.params['id']);
    const country = countriesData.find(el => el.id === countryId);
    res.send(JSON.stringify(country));
  });
});

app.post('/api/countries', express.json(), (req, res) => {
  readFile((data) => {
    const countriesData = JSON.parse(data)
    const newCountryId = countriesData.length + 1;
    const newCountry = {id: newCountryId, ...req.body}
    countriesData.push(newCountry)

    writeFile(JSON.stringify(countriesData, null, 2), () => {
      res.status(200).send(JSON.stringify({text: `Created successfully new country with id ${newCountryId}`}));
    });
  });
});

app.put('/api/countries/:id', express.json(), (req, res) => {
  readFile((data) => {
    const countriesData = JSON.parse(data)
    const countryId = Number(req.params['id']);
    const target = countriesData.find((obj) => Number(obj.id) === countryId)
    const source = {id: countryId, ...req.body}
    Object.assign(target, source);

    writeFile(JSON.stringify(countriesData, null, 2), () => {
      res.status(200).send(JSON.stringify({text: `Updated successfully country with id ${countryId}`}));
    });
  });
});

app.delete('/api/countries/:id', express.json(), (req, res) => {
  readFile((data) => {
    const countriesData = JSON.parse(data)
    const countryId = Number(req.params['id']);
    const filteredCountries = countriesData.filter(el => Number(el.id) !== countryId);

    writeFile(JSON.stringify(filteredCountries, null, 2), () => {
      res.status(200).send(JSON.stringify({text: `Deleted successfully country with id ${countryId}`}));
    });
  });
});


app.listen(PORT, () => {
  console.log(`Example app listening on port ${PORT}`)
})
