import {Injectable} from '@angular/core';
import {CountriesService} from "./countries.service";
import {ActivationStart, Router} from "@angular/router";
import {CountryRequestType} from "../types/CountryRequestType";

@Injectable()
export class PopupService {
  public countryId: number;

  constructor(
    private countriesService: CountriesService,
    private router: Router
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof ActivationStart && event.snapshot) {
        const {id} = event.snapshot.params
        this.countryId = id;
      }
    });
  }

  update(country: CountryRequestType): void {
    this.countriesService.updateCountry(country, this.countryId);
  }

  remove(): void {
    this.countriesService.removeCountry(this.countryId);
    this.close();
  }

  close(): void {
    this.router.navigate(['/'])
  }
}
