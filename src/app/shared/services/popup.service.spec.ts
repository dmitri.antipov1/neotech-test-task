import { TestBed } from '@angular/core/testing';

import { PopupService } from './popup.service';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {CountriesService} from "./countries.service";
import {ApiService} from "./api/api.service";

describe('PopupService', () => {
  let service: PopupService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PopupService,
        CountriesService,
        ApiService
      ]
    });
    service = TestBed.inject(PopupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
