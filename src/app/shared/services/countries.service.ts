import {Injectable} from '@angular/core';
import {ApiService} from "./api/api.service";
import {BehaviorSubject, Observable, switchMap} from "rxjs";
import {Country} from "../types/Country";
import {HttpErrorResponse} from "@angular/common/http";
import {CountryRequestType} from "../types/CountryRequestType";

@Injectable()
export class CountriesService {
  countries$: BehaviorSubject<Country[]> = new BehaviorSubject<Country[]>([]);

  constructor(private apiService: ApiService) {
    this.getCountries();
  }

  getCountries(): void {
    this.apiService.readCountries().subscribe({
      next: (res) => {
        this.countries$.next(res);
      },
      error: (error) => {
        console.log(`Error message: ${error.message}`)
      }
    });
  }

  addCountry(country: CountryRequestType): void {
    this.apiService.createCountry(country).pipe(
      switchMap(() => this.apiService.readCountries())
    ).subscribe({
      next: (res) => {
        this.countries$.next(res);
      },
      error: (error: HttpErrorResponse) => {
        console.log(`Error message: ${error.message}`)
      }
    });
  }

  updateCountry(country: CountryRequestType, countryId: number): void {
    this.apiService.updateCountry(country, countryId).pipe(
      switchMap(() => this.apiService.readCountries())
    ).subscribe({
      next: (res) => {
        this.countries$.next(res);
      },
      error: (error: HttpErrorResponse) => {
        console.log(`Error message: ${error.message}`)
      }
    })
  }

  removeCountry(countryId: number): void {
    this.apiService.deleteCountry(countryId).pipe(
      switchMap(() => this.apiService.readCountries())
    ).subscribe({
      next: (res) => {
        this.countries$.next(res);
      },
      error: (error: HttpErrorResponse) => {
        console.log(`Error message: ${error.message}`)
      }
    })
  }

  getCountry(countryId: number): Observable<Country> {
    return this.apiService.getCountry(countryId);
  }
}
