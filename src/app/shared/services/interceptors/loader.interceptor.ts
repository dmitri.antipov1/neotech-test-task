import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {delay, finalize, Observable} from 'rxjs';
import {LoaderService} from "../loader.service";

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(public loaderService: LoaderService) { }

  public intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    this.loaderService.toggle(true);

    // made a small delay for the correct display of the spinner
    return next.handle(req).pipe(
      delay(500),
      finalize(() => this.loaderService.toggle(false)),
    );
  }
}
