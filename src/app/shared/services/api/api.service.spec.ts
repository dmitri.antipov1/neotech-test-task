import { TestBed } from '@angular/core/testing';

import { ApiService } from './api.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {CountryRequestType} from "../../types/CountryRequestType";

const fakeCountry = {
  id: 1,
  country_name: 'Country name',
  gross_domestic_product: 1.2,
  population: 10000000,
  currency: 'Dollar',
  president: 'Fake fake',
  average_salary: 10000,
  about: 'fake info',
}

const fakeCountryWithoutId: CountryRequestType = {
  country_name: 'Country name',
  gross_domestic_product: 1.2,
  population: 10000000,
  currency: 'Dollar',
  president: 'Fake fake',
  average_salary: 10000,
  about: 'fake info',
}

describe('ApiService', () => {
  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
    service = TestBed.inject(ApiService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should delete country', () => {
    service.deleteCountry(fakeCountry.id).subscribe(() => {
      expect(fakeCountry.id).toBe(1);
    });
    const request = httpMock.expectOne(`${service.baseUrl}api/countries/${fakeCountry.id}`);
    expect(request.request.method).toBe('DELETE');
    request.flush(fakeCountry);
  });

  it('should create country', () => {
    service.createCountry(fakeCountryWithoutId).subscribe(() => {
      expect(fakeCountry.id).toBe(1);
    });
    const request = httpMock.expectOne(`${service.baseUrl}api/countries`);
    expect(request.request.method).toBe('POST');
    request.flush(fakeCountry);
  });

  it('should update country', () => {
    const needToUpdateCountry = {...fakeCountry, president: 'Updated president'}
    service.updateCountry(needToUpdateCountry, needToUpdateCountry.id).subscribe(() => {
      expect(needToUpdateCountry.president).toBe('Updated president');
    });
    const request = httpMock.expectOne(`${service.baseUrl}api/countries/${needToUpdateCountry.id}`);
    expect(request.request.method).toBe('PUT');
    request.flush(needToUpdateCountry);
  });

  it('should read countries', () => {
    service.readCountries().subscribe((res) => {
      expect(res).toEqual([fakeCountry]);
    });
    const request = httpMock.expectOne(`${service.baseUrl}api/countries`);
    expect(request.request.method).toBe('GET');
    request.flush([fakeCountry]);
  });

  it('should read country', () => {
    service.getCountry(1).subscribe((res) => {
      expect(res.id).toBe(1);
    });
    const request = httpMock.expectOne(`${service.baseUrl}api/countries/${1}`);
    expect(request.request.method).toBe('GET');
    request.flush(fakeCountry);
  });
});
