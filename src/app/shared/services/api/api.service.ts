import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {Observable} from "rxjs";
import {Country} from "../../types/Country";
import {CountryRequestType} from "../../types/CountryRequestType";
import {ServerResponseInterface} from "../../types/ServerResponse.interface";

@Injectable()
export class ApiService {
  public baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) {}

  createCountry(country: CountryRequestType): Observable<ServerResponseInterface> {
    return this.http.post<ServerResponseInterface>(`${this.baseUrl}api/countries/`, country);
  }

  readCountries(): Observable<Country[]> {
    return this.http.get<Country[]>(`${this.baseUrl}api/countries/`);
  }

  updateCountry(country: CountryRequestType, countryId: number): Observable<ServerResponseInterface> {
    return this.http.put<ServerResponseInterface>(`${this.baseUrl}api/countries/${countryId}`, country);
  }

  deleteCountry(removeId: number): Observable<ServerResponseInterface> {
    return this.http.delete<ServerResponseInterface>(`${this.baseUrl}api/countries/${removeId}`);
  }

  getCountry(countryId: number): Observable<Country> {
    return this.http.get<Country>(`${this.baseUrl}api/countries/` + countryId);
  }
}
