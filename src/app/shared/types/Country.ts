export interface Country {
  id: number;
  country_name: string;
  gross_domestic_product: number;
  population: number,
  currency: string,
  president: string,
  average_salary: number,
  about: string,
}
