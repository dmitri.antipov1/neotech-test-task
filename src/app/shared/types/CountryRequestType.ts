import {Country} from "./Country";

export type CountryRequestType = Omit<Country, 'id'>
