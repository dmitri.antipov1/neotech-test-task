export type NavigationParamsType = {
  id: string;
  state: string;
}
