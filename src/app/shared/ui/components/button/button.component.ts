import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css'],
})
export class ButtonComponent {
  @Input() specialButtonText: string | null = null;
  @Input() specialSize: string | null = null;
  @Input() isVisible: boolean = false;
  @Input() disabled: boolean = false;
  @Input() specialBGColor = 'var(--middle-green)'
  @Input() specialBorderRadius: string = '10px'
  @Input() specialFontSize: string = '16px'
  @Output() handleEvent = new EventEmitter();
}
