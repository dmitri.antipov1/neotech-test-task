import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Country} from 'src/app/shared/types/Country';
import {CountryRequestType} from "../../../types/CountryRequestType";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormComponent implements OnChanges {
  @Input() country: Country | null = null;
  @Output() valuesEmitter = new EventEmitter<CountryRequestType>();
  public form: FormGroup;
  private numbersPattern = '^[0-9]+(.[0-9]{0,2})?$';

  constructor(private fb: FormBuilder) {
    this.initializeValues();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (this.country && changes['country'] && changes['country'].currentValue) {
      this.getCurrentModel(this.country);
    }
  }

  private initializeValues(): void {
    this.form = this.fb.group({
      country_name: ['', Validators.required],
      gross_domestic_product: ['', [Validators.required, Validators.pattern(this.numbersPattern)]],
      population: ['', [Validators.required, Validators.pattern(this.numbersPattern)]],
      currency: ['', Validators.required],
      president: ['', Validators.required],
      average_salary: ['', [Validators.required, Validators.pattern(this.numbersPattern)]],
      about: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(300)]],
    });
  }

  public getControl(controlName: string): FormControl {
    return this.form.controls[controlName] as FormControl;
  }

  public send(): void {
    if (this.form.valid) {
      this.valuesEmitter.emit(this.form.value);
    }
  }

  private getCurrentModel(country: Country): void {
    this.form.patchValue({
      country_name: country.country_name,
      gross_domestic_product: country.gross_domestic_product,
      population: country.population,
      currency: country.currency,
      president: country.president,
      average_salary: country.average_salary,
      about: country.about
    })
  }
}
