import {ChangeDetectionStrategy, Component, ElementRef, HostListener, Input, ViewChild} from '@angular/core';
import {PopupService} from "../../../services/popup.service";

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupComponent {
  @Input() title: string = '';
  @Input() state: boolean = false;
  @ViewChild('modalMaskRef') public modalMaskRef: ElementRef | undefined

  constructor(private popupService: PopupService) {}

  @HostListener('window:keyup.esc')
  keyEvent(event: KeyboardEvent): void {
    this.popupService.close();
  }

  clickOutside($event: Event): void {
    if (this.modalMaskRef?.nativeElement == $event.target) this.popupService.close();
  }

  close() {
    this.popupService.close();
  }
}
