import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {PopupService} from "../../../../services/popup.service";
import {CountryRequestType} from "../../../../types/CountryRequestType";
import {Country} from "../../../../types/Country";
import {CountriesService} from "../../../../services/countries.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-popup-edit',
  templateUrl: './popup-edit.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupEditComponent implements OnInit {
  public country$: Observable<Country>;

  constructor(private popupService: PopupService, private countriesService: CountriesService) {
  }

  ngOnInit(): void {
    this.country$ = this.countriesService.getCountry(this.popupService.countryId);
  }

  public updateCountry(country: CountryRequestType) {
    this.popupService.update(country);
    this.popupService.close();
  }
}
