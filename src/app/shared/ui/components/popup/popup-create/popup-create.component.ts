import {Component} from '@angular/core';
import {CountriesService} from "../../../../services/countries.service";
import {CountryRequestType} from "../../../../types/CountryRequestType";
import {PopupService} from "../../../../services/popup.service";

@Component({
  selector: 'app-popup-create',
  templateUrl: './popup-create.component.html'
})
export class PopupCreateComponent {

  constructor(
    private popupService: PopupService,
    private countriesService: CountriesService
  ) {}

  createCountry(country: CountryRequestType) {
    this.countriesService.addCountry(country);
    this.popupService.close();
  }
}
