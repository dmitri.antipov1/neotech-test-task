import {Component} from '@angular/core';
import {PopupService} from "../../../../services/popup.service";

@Component({
  selector: 'app-popup-remove',
  templateUrl: './popup-remove.component.html',
  styleUrls: ['./popup-remove.component.css']
})
export class PopupRemoveComponent {
  public buttonText = 'Remove';
  public buttonColor = 'var(--red)';

  constructor(private popupService: PopupService) {}

  remove() {
    this.popupService.remove()
  }
}
