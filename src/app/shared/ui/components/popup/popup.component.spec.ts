import {PopupComponent} from "./popup.component";
import {ComponentFixture, fakeAsync, TestBed, tick} from "@angular/core/testing";
import {NO_ERRORS_SCHEMA} from "@angular/core";
import {PopupService} from "../../../services/popup.service";
import {CountriesService} from "../../../services/countries.service";
import {ApiService} from "../../../services/api/api.service";
import {HttpClientTestingModule} from "@angular/common/http/testing";

describe('Popup component test', () => {
  let component: PopupComponent;
  let service: PopupService;
  let fixture: ComponentFixture<PopupComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ApiService,
        PopupService,
        CountriesService],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(PopupComponent);
    service = TestBed.inject(PopupService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close popup when clicked outside popup', fakeAsync(() => {
    const spy = spyOn(service, 'close').and.callThrough();
    const event = new KeyboardEvent('click');
    component.modalMaskRef?.nativeElement.dispatchEvent(event);
    tick();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalled();
  }));

  it('should close popup when was pressed ESC', fakeAsync(() => {
    const spy = spyOn(service, 'close');
    const eventMock = new KeyboardEvent('keyup', {key: 'Escape'});
    component.keyEvent(eventMock)
    expect(spy).toHaveBeenCalled()
    tick();
  }));

})
