import {Component} from '@angular/core';

@Component({
  selector: 'app-close-icon',
  template: `
    <svg width="25" height="25" viewBox="0 0 40 45" fill="none" xmlns="http://www.w3.org/2000/svg"
         style="cursor: pointer; pointer-events: all">
      <path d="M24 18.6897L16 26.6897" stroke="black" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round" />
      <path d="M16 18.6897L24 26.6897" stroke="black" stroke-width="2" stroke-linecap="round"
            stroke-linejoin="round" />
    </svg>`
})
export class CloseIconComponent {
}
