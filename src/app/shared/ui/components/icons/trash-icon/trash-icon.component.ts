import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-trash-icon',
  template: `
    <svg style="cursor: pointer" width="25" height="25" viewBox="0 0 70 70" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M15.3125 15.3125L18.0469 59.0625C18.1768 61.5904 20.0156 63.4375 22.4219 63.4375H47.5781C49.9939 63.4375 51.7986 61.5904 51.9531 59.0625L54.6875 15.3125"
        [attr.stroke]="color" stroke-width="4.375" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M10.9375 15.3125H59.0625" [attr.stroke]="color" stroke-width="4.375" stroke-miterlimit="10"
            stroke-linecap="round"/>
      <path
        d="M26.25 15.3125V9.84375C26.2487 9.41249 26.3327 8.98525 26.4972 8.58658C26.6616 8.18791 26.9033 7.82568 27.2082 7.52074C27.5132 7.21579 27.8754 6.97415 28.2741 6.8097C28.6727 6.64525 29.1 6.56124 29.5312 6.5625H40.4688C40.9 6.56124 41.3273 6.64525 41.7259 6.8097C42.1246 6.97415 42.4868 7.21579 42.7918 7.52074C43.0967 7.82568 43.3384 8.18791 43.5028 8.58658C43.6673 8.98525 43.7513 9.41249 43.75 9.84375V15.3125M35 24.0625V54.6875M25.1562 24.0625L26.25 54.6875M44.8438 24.0625L43.75 54.6875"
        [attr.stroke]="color" stroke-width="4.375" stroke-linecap="round" stroke-linejoin="round"/>
    </svg>`
})
export class TrashIconComponent {
  @Input() color: string = 'black'
}
