import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-arrow-icon',
  template: `
    <div
      (click)="buttonEvent.emit()"
      [ngStyle]="fixedPosition ? fixedStyles : staticStyles">
      <svg [attr.width]="size" [attr.height]="size" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M10 14C10.2833 14 10.521 13.904 10.713 13.712C10.9043 13.5207 11 13.2833 11 13V9.8L11.9 10.7C12.0833 10.8833 12.3167 10.975 12.6 10.975C12.8833 10.975 13.1167 10.8833 13.3 10.7C13.4833 10.5167 13.575 10.2833 13.575 10C13.575 9.71667 13.4833 9.48333 13.3 9.3L10.7 6.7C10.6 6.6 10.4917 6.529 10.375 6.487C10.2583 6.44567 10.1333 6.425 10 6.425C9.86667 6.425 9.74167 6.44567 9.625 6.487C9.50833 6.529 9.4 6.6 9.3 6.7L6.7 9.3C6.51667 9.48333 6.425 9.71667 6.425 10C6.425 10.2833 6.51667 10.5167 6.7 10.7C6.88333 10.8833 7.11667 10.975 7.4 10.975C7.68333 10.975 7.91667 10.8833 8.1 10.7L9 9.8V13C9 13.2833 9.096 13.5207 9.288 13.712C9.47933 13.904 9.71667 14 10 14ZM10 20C8.61667 20 7.31667 19.7373 6.1 19.212C4.88333 18.6873 3.825 17.975 2.925 17.075C2.025 16.175 1.31267 15.1167 0.788 13.9C0.262667 12.6833 0 11.3833 0 10C0 8.61667 0.262667 7.31667 0.788 6.1C1.31267 4.88333 2.025 3.825 2.925 2.925C3.825 2.025 4.88333 1.31233 6.1 0.787C7.31667 0.262333 8.61667 0 10 0C11.3833 0 12.6833 0.262333 13.9 0.787C15.1167 1.31233 16.175 2.025 17.075 2.925C17.975 3.825 18.6873 4.88333 19.212 6.1C19.7373 7.31667 20 8.61667 20 10C20 11.3833 19.7373 12.6833 19.212 13.9C18.6873 15.1167 17.975 16.175 17.075 17.075C16.175 17.975 15.1167 18.6873 13.9 19.212C12.6833 19.7373 11.3833 20 10 20Z"
          [attr.fill]="color"/>
      </svg>
    </div>
  `
})
export class ArrowIconComponent {
  @Input() size: string = '20';
  @Input() color: string = 'black';
  @Input() fixedPosition = false;
  @Output() buttonEvent = new EventEmitter();
  public fixedStyles = {'cursor': 'pointer', 'position': 'fixed', 'bottom': '15px', 'left': '15px',}
  public staticStyles = {'position': 'static'}
}
