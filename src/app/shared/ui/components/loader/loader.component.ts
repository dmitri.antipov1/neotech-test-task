import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-loader',
  template: `
    <div class="overlay">
      <div class="lds-ring"
           [style.width]="size"
           [style.height]="size"
      >
        <div
          [style.width]="size"
          [style.height]="size"
          [style.border]="'8px solid ' + color"
          [style.border-color]="color + ' transparent transparent transparent'"></div>
        <div
          [style.width]="size"
          [style.height]="size"
          [style.border]="'8px solid ' + color"
          [style.border-color]="color + ' transparent transparent transparent'"></div>
        <div
          [style.width]="size"
          [style.height]="size"
          [style.border]="'8px solid ' + color"
          [style.border-color]="color + ' transparent transparent transparent'"></div>
        <div
          [style.width]="size"
          [style.height]="size"
          [style.border]="'8px solid ' + color"
          [style.border-color]="color + ' transparent transparent transparent'"></div>
      </div>
    </div>`,
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent {
  @Input() size: string;
  @Input() color: string;
}
