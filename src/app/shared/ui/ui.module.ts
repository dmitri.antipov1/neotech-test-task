import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrashIconComponent} from './components/icons/trash-icon/trash-icon.component';
import {EditIconComponent} from './components/icons/edit-icon/edit-icon.component';
import {ButtonComponent} from './components/button/button.component';
import {PopupComponent} from './components/popup/popup.component';
import {CloseIconComponent} from './components/icons/close-icon/close-icon.component';
import {InputComponent} from './components/input/input.component';
import {ReactiveFormsModule} from "@angular/forms";
import {TextareaComponent} from './components/textarea/textarea.component';
import {LoaderComponent} from './components/loader/loader.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {ArrowIconComponent} from './components/icons/arrow-icon/arrow-icon.component';
import { PopupEditComponent } from './components/popup/popup-edit/popup-edit.component';
import {FormComponent} from "./components/form/form.component";
import { PopupRemoveComponent } from './components/popup/popup-remove/popup-remove.component';
import { PopupCreateComponent } from './components/popup/popup-create/popup-create.component';


@NgModule({
  declarations: [
    TrashIconComponent,
    EditIconComponent,
    ButtonComponent,
    PopupComponent,
    CloseIconComponent,
    InputComponent,
    TextareaComponent,
    LoaderComponent,
    ArrowIconComponent,
    PopupEditComponent,
    FormComponent,
    PopupRemoveComponent,
    PopupCreateComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  exports: [
    TrashIconComponent,
    EditIconComponent,
    ButtonComponent,
    PopupComponent,
    InputComponent,
    TextareaComponent,
    LoaderComponent,
    ArrowIconComponent,
    FormComponent,
  ]
})
export class UiModule {
}
