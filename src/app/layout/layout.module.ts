import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './components/header/header.component';
import {TableComponent} from './components/table/table.component';
import {UiModule} from "../shared/ui/ui.module";
import {MainComponent} from './components/main/main.component';
import {PopupService} from "../shared/services/popup.service";
import {ReactiveFormsModule} from "@angular/forms";
import {CountriesService} from "../shared/services/countries.service";
import {LoaderService} from "../shared/services/loader.service";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    HeaderComponent,
    TableComponent,
    MainComponent,
  ],
  exports: [
    MainComponent
  ],
  imports: [
    CommonModule,
    UiModule,
    ReactiveFormsModule,
    RouterModule.forChild([])
  ],
  providers: [
    PopupService,
    CountriesService,
    LoaderService,
  ]
})
export class LayoutModule { }
