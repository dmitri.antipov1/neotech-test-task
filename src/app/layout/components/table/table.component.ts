import {Component, OnInit} from '@angular/core';
import {PopupService} from "../../../shared/services/popup.service";
import {BehaviorSubject} from "rxjs";
import {CountriesService} from "../../../shared/services/countries.service";
import {Country} from "../../../shared/types/Country";
import {LoaderService} from "../../../shared/services/loader.service";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  public isCreationForm: boolean = false;
  public selectedCountryIndex: number | null = null;
  public countries$: BehaviorSubject<Country[]>;
  public isLoading$: BehaviorSubject<boolean>;
  public tableHeaders: string[] = [
    'ID',
    'Country',
    'GDP',
    'Population',
    'Currency',
    'President',
    'Average Salary ($)',
    'About',
    'Actions'
  ]

  constructor(
    private popupService: PopupService,
    private countriesService: CountriesService,
    private loaderService: LoaderService,
  ) {}

  ngOnInit(): void {
    this.initializeValues();
  }

  country(index: number, item: Country){
    return item.id;
  }

  initializeValues(): void {
    this.countries$ = this.countriesService.countries$;
    this.isLoading$ = this.loaderService.isLoading$;
  }
}
