import {Component, OnInit} from '@angular/core';
import {fadeAnimation, fadeInOutAnimation} from "./animations";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    fadeAnimation,
    fadeInOutAnimation
  ]
})
export class MainComponent implements OnInit {
  public isScrolledPage = false;

  ngOnInit() {
    window.addEventListener('scroll', () => {
      this.isScrolledPage = Boolean(window.pageYOffset);
    });
  }

  scrollToTop(): void {
    window.scrollTo({top: 0, behavior: 'smooth'});
  }
}
