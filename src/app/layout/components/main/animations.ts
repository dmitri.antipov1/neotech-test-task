import {animate, query, style, transition, trigger} from "@angular/animations";

export const fadeInOutAnimation = trigger('fadeInOutAnimation', [
  transition('* => *', [
    query(':enter', [style({ opacity: 0, position: 'absolute' })], {
      optional: true,
    }),
    query(
      ':leave',
      [
        style({ opacity: 1 }),
        animate('0.3s', style({ opacity: 0, position: 'absolute' })),
      ],
      { optional: true }
    ),
    query(
      ':enter',
      [
        style({ opacity: 0 }),
        animate('0.3s', style({ opacity: 1, position: 'relative' })),
      ],
      { optional: true }
    ),
  ]),
]);

export const fadeAnimation = trigger('fade', [
  transition('void => *', [
    style({opacity: 0}),
    animate(1000, style({opacity: 1}))
  ])
])
