import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Country} from 'src/app/shared/types/Country';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddFormComponent implements OnInit {
  @Input() country: Country | null = null;
  @Output() valuesEmitter = new EventEmitter();
  public form: FormGroup;
  private numbersPattern = '^[0-9]+(.[0-9]{0,2})?$';

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initializeValues();
  }

  private initializeValues(): void {
    this.form = this.fb.group({
      country_name: [this.country ? this.country.country_name : '',
        Validators.required],
      gross_domestic_product: [this.country ? this.country.gross_domestic_product : '',
        [Validators.required, Validators.pattern(this.numbersPattern)]],
      population: [this.country ? this.country.population : '',
        [Validators.required, Validators.pattern(this.numbersPattern)]],
      currency: [this.country ? this.country.currency : '',
        Validators.required],
      president: [this.country ? this.country.president : '',
        Validators.required],
      average_salary: [this.country ? this.country.average_salary : '',
        [Validators.required, Validators.pattern(this.numbersPattern)]],
      about: [this.country ? this.country.about : '',
        [Validators.required, Validators.minLength(2), Validators.maxLength(200)]],
    });
  }

  public getControl(controlName: string): FormControl {
    return this.form.controls[controlName] as FormControl;
  }

  public send(): void {
    if (this.form.valid) {
      this.valuesEmitter.emit(this.form.value);
    }
  }
}
