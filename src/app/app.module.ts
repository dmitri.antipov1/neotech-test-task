import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {LayoutModule} from "./layout/layout.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {ApiService} from "./shared/services/api/api.service";
import {LoaderInterceptor} from "./shared/services/interceptors/loader.interceptor";
import {RouterModule, Routes} from "@angular/router";
import {MainComponent} from "./layout/components/main/main.component";
import {PopupCreateComponent} from "./shared/ui/components/popup/popup-create/popup-create.component";
import {PopupRemoveComponent} from "./shared/ui/components/popup/popup-remove/popup-remove.component";
import {PopupEditComponent} from "./shared/ui/components/popup/popup-edit/popup-edit.component";

const routes: Routes = [
  {path: '',  component: MainComponent, children: [
      {path: 'country/create', component: PopupCreateComponent},
      {path: 'country/remove/:id', component: PopupRemoveComponent},
      {path: 'country/edit/:id', component: PopupEditComponent},
    ]},
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    LayoutModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ApiService,
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
